#include <postgresql/libpq-fe.h>
#include <iostream>

enum CODES_ERREUR
{
  OK_TOUT_VA_BIEN,
  ERR_NO_PING,
  PROGRAMME_TERMINE,
  CONNEXION_ECHOUEE,
  CONNEXION_ETABLIE,
};

int main()
{
  int code_retour;
  //const char requete_sql[] = "SELECT * FROM animal";
  char informations_de_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=t.lehugeur user=t.lehugeur password=P@ssword connect_timeout=4";
  PGPing code_retour_ping;
  PGconn *connexion;
  PGresult *resultat_requete;
  ExecStatusType etat_resultat;
  ConnStatusType code_retour_connexion;
  char *reponse_requete;
  code_retour = OK_TOUT_VA_BIEN;
  code_retour_ping = PQping(informations_de_connexion);

  if(code_retour_ping == PQPING_OK)
  {
    //ETABLISSEMENT DE LA CONNEXION OU AFFICHAGE DES ERREURS
    std::cout << "Connexion possible" << std::endl;
    //MAINTENANT IL FAUT SE CONNECTER
    connexion = PQconnectdb(informations_de_connexion);
    code_retour_connexion = PQstatus(connexion); // statut de la connexion

    if(code_retour_connexion == CONNECTION_OK) //TU ES CONNECTÉ ???
    {
      //traitement
      std::cout << "Connexion etablie" << std::endl;
      code_retour = CONNEXION_ETABLIE;
      resultat_requete = PQexec(connexion, "SET SCHEMA 'si6' ;SELECT * FROM \"Animal\"");
      etat_resultat = PQresultStatus(resultat_requete);

      if(etat_resultat == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée au serveur était vide. " << std::endl;
      }
      else if(etat_resultat == PGRES_COMMAND_OK)
      {
        std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
      }
      else if(etat_resultat == PGRES_TUPLES_OK)
      {
        std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)." << std::endl;
      }
      else if(etat_resultat == PGRES_COPY_OUT)
      {
        std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
      }
      else if(etat_resultat == PGRES_COPY_IN)
      {
        std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
      }
      else if(etat_resultat == PGRES_BAD_RESPONSE)
      {
        std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(etat_resultat == PGRES_NONFATAL_ERROR)
      {
        std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
      }
      else if(etat_resultat == PGRES_FATAL_ERROR)
      {
        std::cerr << "Une erreur fatale est survenue." << std::endl;
      }
      else if(etat_resultat == PGRES_COPY_BOTH)
      {
        std::cout << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, so this status should not occur in ordinary applications" << std::endl;
      }
      else if(etat_resultat == PGRES_SINGLE_TUPLE)
      {
        std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête" << std::endl;
      }

      //fin de la connexion
      //debut affichage
      std::cout << "La connexion au serveur de base de données 'postgresql.bts-malraux72.net a été établie avec les parametres suivants :" << std::endl;
      std::cout << "* utilisateur : t.lehugeur " << std::endl;
      std::cout << "* mot de passe : ********" << std::endl;
      std::cout << "* base de données : thomas.lehugeur" << std::endl;
      std::cout << "* port TCP : 5432" << std::endl;
      std::cout << "* chiffrement SSL : true" << std::endl;
      std::cout << "* encodage : UTF8" << std::endl;
      std::cout << "* version du protocole : 3" << std::endl;
      std::cout << "* version du serveur : 90610" << std::endl;
      std::cout << "* version de la bibliothèque 'libpq' du client : 110001" << std::endl;
      std::cout << "Le resultat de la requete est située à l'adresse suivante : \n " << resultat_requete << std::endl;
      int nbLignes;
      int nbColonnes;
      int l,c;
      char *nom_colonne;
      // reponse_requete =  PQgetvalue(resultat_requete, 2, 2);//Le programme va aller chercher la cellule en 2-2 de la reponse de la requete envoyée.
      nbLignes = PQntuples(resultat_requete);
      nbColonnes = PQnfields(resultat_requete);
      
      std::cout << "nombre de lignes: " << nbLignes << std::endl;
      std::cout << "nombre de colonnes: " << nbColonnes << std::endl;
      std::cout << "réponse : " <<  std::endl;

      for(c=0; c< nbColonnes; c++){
        nom_colonne = PQfname(resultat_requete, c);
        std::cout << nom_colonne << " | " << std::flush;
      }
        std:: cout << "\n" << std::endl;
      
      for(l=0; l < nbLignes; l++){
        for(c=0; c < nbColonnes; c++){
          reponse_requete = PQgetvalue(resultat_requete, l, c);
          std::cout << reponse_requete << " | " << std::flush;
        }
        std::cout << "\n" << std::endl;
      }
      
    }
    else
    {
      std::cerr << "La connexion n'a pas pu être établie" << std::endl;
      code_retour = CONNEXION_ECHOUEE;
    }
  }
  else
  {
    std::cerr << "le serveur n'est pas accessible pour le moment, veuillez contacter votre administrateur reseau" << std::endl;
    code_retour = ERR_NO_PING;
  }

  return code_retour;
}
